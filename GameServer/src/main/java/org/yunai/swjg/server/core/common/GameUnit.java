package org.yunai.swjg.server.core.common;

/**
 * 游戏基本单元接口
 * User: yunai
 * Date: 13-5-2
 * Time: 下午7:59
 */
public interface GameUnit {

    /**
     * 设置游戏单元编号
     * @param id 编号
     */
    void setId(Integer id);

    /**
     * @return 获得游戏单元编号
     */
    Integer getId();
}
