package org.yunai.swjg.server.module.quest.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.quest.QuestService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_FinishQuestReq;

import javax.annotation.Resource;

/**
 * 完成任务消息命令
 * User: yunai
 * Date: 13-5-11
 * Time: 上午10:35
 */
@Controller
public class FinishQuestCommand extends GameMessageCommand<C_S_FinishQuestReq> {

    @Resource
    private QuestService questService;

    @Override
    public void execute(Online online, C_S_FinishQuestReq msg) {
        questService.finish(online, msg.getQuestId());
    }
}
