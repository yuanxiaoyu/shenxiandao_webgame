package org.yunai.swjg.server.module.activity;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.template.ActivityTemplate;

/**
 * 活动基类
 * User: yunai
 * Date: 13-5-20
 * Time: 下午6:53
 */
public abstract class AbstractActivity {

    private final ActivityTemplate activityTemplate;
    private volatile ActivityDef.Status status;

    public AbstractActivity(Integer actId) {
        this.activityTemplate = ActivityTemplate.get(actId);
        this.status = ActivityDef.Status.END;
    }

    public abstract void join(Online online);

    public abstract void prepare();

    public abstract void process();

    public abstract void end();

    public boolean isEnd() {
        return status == ActivityDef.Status.END;
    }

    public ActivityDef.Status getStatus() {
        return status;
    }

    public void setStatus(ActivityDef.Status status) {
        this.status = status;
    }

    public ActivityTemplate getActivityTemplate() {
        return activityTemplate;
    }

    public Integer getId() {
        return activityTemplate.getId();
    }

    /**
     * 检查当前状态是否能转化成目标状态
     *
     * @param targetStatus 目标状态
     * @return 当前状态是否能转化成目标状态
     */
    public boolean checkStatus(ActivityDef.Status targetStatus) {
        return targetStatus.valid(status);
    }
}
