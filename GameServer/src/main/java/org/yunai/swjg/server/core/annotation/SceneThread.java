package org.yunai.swjg.server.core.annotation;

import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 * 场景线程调用方法注解
 * User: yunai
 * Date: 13-4-25
 * Time: 下午7:09
 */
@Target(METHOD)
public @interface SceneThread {
}
