package org.yunai.swjg.server.core.session;

import org.apache.mina.core.session.IoSession;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.yfserver.plugin.mina.session.AbstractMinaSession;

/**
 * 游戏会话
 * User: yunai
 * Date: 13-3-26
 * Time: 下午4:01
 */
public class GameSession extends AbstractMinaSession {

    private volatile Online online;

    public GameSession(IoSession session) {
        super(session);
    }

    public Online getOnline() {
        return online;
    }

    public void setOnline(Online online) {
        this.online = online;
    }
}
