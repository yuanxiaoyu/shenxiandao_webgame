package org.yunai.swjg.server.module.arena;

import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.arena.vo.ArenaRank;
import org.yunai.swjg.server.module.player.vo.Player;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * 竞技场Service
 * User: yunai
 * Date: 13-5-26
 * Time: 下午11:21
 */
@Service
public class ArenaService {

    /**
     * 竞技场Runner
     */
    private final ArenaRunner runner;
    /**
     * 竞技场
     */
    private final Arena arena;
    private Map<Integer, ArenaRank> ranks;

    public ArenaService() {
        this.arena = new Arena();
        this.runner = new ArenaRunner(arena);
    }

    public ArenaRunner getRunner() {
        return runner;
    }

    /**
     * 开服的时候将竞技场排名读取到内存
     */
    @PostConstruct
    public void init() {
        ranks = new HashMap<>();
    }

    public void detail(Online online) {
        Player player = online.getPlayer();
        ArenaRank rankInfo = ranks.get(player.getId());
        if (rankInfo == null) {
            rankInfo = firstInit(online);
        }
    }

    private ArenaRank firstInit(Online online) {
        ArenaRank arenaInfo = ArenaRank.save(online, ranks.size() + 1);
        ranks.put(arenaInfo.getId(), arenaInfo);
        return arenaInfo;
    }
}
