package org.yunai.swjg.server.module.activity.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.ActivityService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_ActivityJoinReq;

import javax.annotation.Resource;

/**
 * 活动加入消息命令
 * User: yunai
 * Date: 13-5-20
 * Time: 下午7:22
 */
@Controller
public class ActivityJoinCommand extends GameMessageCommand<C_S_ActivityJoinReq> {

    @Resource
    private ActivityService activityService;

    @Override
    protected void execute(Online online, C_S_ActivityJoinReq msg) {
        activityService.join(online, msg.getActId().intValue());
    }
}