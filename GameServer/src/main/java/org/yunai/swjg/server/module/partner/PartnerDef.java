package org.yunai.swjg.server.module.partner;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 伙伴枚举
 * User: yunai
 * Date: 13-5-30
 * Time: 下午8:11
 */
public interface PartnerDef {

    public static enum Status implements IndexedEnum {
        /**
         * 雇佣
         */
        RECRUIT((byte) 1),
        /**
         * 解雇
         */
        FIRE((byte) 2),
        /**
         * 在客栈，未雇佣过
         */
        INN((byte) 3);

        private final byte status;

        private Status(byte status) {
            this.status = status;
        }

        @Override
        public int getIndex() {
            return status;
        }

        public byte get() {
            return status;
        }

        private static final List<Status> VALUES = Util.toIndexes(values());

        public static Status valueOf(byte status) {
            return Util.valueOf(VALUES, status);
        }
    }
}
