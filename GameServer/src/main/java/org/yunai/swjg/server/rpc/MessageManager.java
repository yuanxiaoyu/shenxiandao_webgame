package org.yunai.swjg.server.rpc;

import org.yunai.swjg.server.rpc.message.C_S.*;
import org.yunai.swjg.server.rpc.message.S_C.*;
import org.yunai.yfserver.message.AbstractDecoder;
import org.yunai.yfserver.message.AbstractEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 消息管理器
 * User: yunai
 * Date: 13-3-14
 * Time: 下午3:24
 */
public class MessageManager {

    private static Map<Short, AbstractDecoder> decoderMap;
    private static Map<Short, AbstractEncoder> encoderMap;

    static {
        decoderMap = new HashMap<Short, AbstractDecoder>();
        encoderMap = new HashMap<Short, AbstractEncoder>();

        decoderMap.put(S_C_SysMessageReq.CODE, S_C_SysMessageReq.Decoder.getInstance());
        encoderMap.put(S_C_SysMessageReq.CODE, S_C_SysMessageReq.Encoder.getInstance());
        decoderMap.put(S_C_RoleIntInfoReq.CODE, S_C_RoleIntInfoReq.Decoder.getInstance());
        encoderMap.put(S_C_RoleIntInfoReq.CODE, S_C_RoleIntInfoReq.Encoder.getInstance());
        decoderMap.put(S_C_RoleStringInfoReq.CODE, S_C_RoleStringInfoReq.Decoder.getInstance());
        encoderMap.put(S_C_RoleStringInfoReq.CODE, S_C_RoleStringInfoReq.Encoder.getInstance());
        decoderMap.put(C_S_BattleRepMonsterReq.CODE, C_S_BattleRepMonsterReq.Decoder.getInstance());
        encoderMap.put(C_S_BattleRepMonsterReq.CODE, C_S_BattleRepMonsterReq.Encoder.getInstance());
        decoderMap.put(S_C_BattleActionResp.CODE, S_C_BattleActionResp.Decoder.getInstance());
        encoderMap.put(S_C_BattleActionResp.CODE, S_C_BattleActionResp.Encoder.getInstance());
        decoderMap.put(S_C_MonsterListResp.CODE, S_C_MonsterListResp.Decoder.getInstance());
        encoderMap.put(S_C_MonsterListResp.CODE, S_C_MonsterListResp.Encoder.getInstance());
        decoderMap.put(S_C_MonsterDelResp.CODE, S_C_MonsterDelResp.Decoder.getInstance());
        encoderMap.put(S_C_MonsterDelResp.CODE, S_C_MonsterDelResp.Encoder.getInstance());
        decoderMap.put(S_C_MonsterHpReduceResp.CODE, S_C_MonsterHpReduceResp.Decoder.getInstance());
        encoderMap.put(S_C_MonsterHpReduceResp.CODE, S_C_MonsterHpReduceResp.Encoder.getInstance());
        decoderMap.put(C_S_RepEnterReq.CODE, C_S_RepEnterReq.Decoder.getInstance());
        encoderMap.put(C_S_RepEnterReq.CODE, C_S_RepEnterReq.Encoder.getInstance());
        decoderMap.put(C_S_RepLeaveReq.CODE, C_S_RepLeaveReq.Decoder.getInstance());
        encoderMap.put(C_S_RepLeaveReq.CODE, C_S_RepLeaveReq.Encoder.getInstance());
        decoderMap.put(S_C_RepEnterSuccessResp.CODE, S_C_RepEnterSuccessResp.Decoder.getInstance());
        encoderMap.put(S_C_RepEnterSuccessResp.CODE, S_C_RepEnterSuccessResp.Encoder.getInstance());
        decoderMap.put(S_C_RepLeaveResp.CODE, S_C_RepLeaveResp.Decoder.getInstance());
        encoderMap.put(S_C_RepLeaveResp.CODE, S_C_RepLeaveResp.Encoder.getInstance());
        decoderMap.put(S_C_RepCompleteResp.CODE, S_C_RepCompleteResp.Decoder.getInstance());
        encoderMap.put(S_C_RepCompleteResp.CODE, S_C_RepCompleteResp.Encoder.getInstance());
        decoderMap.put(S_C_RepPrizeInfoResp.CODE, S_C_RepPrizeInfoResp.Decoder.getInstance());
        encoderMap.put(S_C_RepPrizeInfoResp.CODE, S_C_RepPrizeInfoResp.Encoder.getInstance());
        decoderMap.put(C_S_SceneEnterReq.CODE, C_S_SceneEnterReq.Decoder.getInstance());
        encoderMap.put(C_S_SceneEnterReq.CODE, C_S_SceneEnterReq.Encoder.getInstance());
        decoderMap.put(S_C_SceneEnterSuccessResp.CODE, S_C_SceneEnterSuccessResp.Decoder.getInstance());
        encoderMap.put(S_C_SceneEnterSuccessResp.CODE, S_C_SceneEnterSuccessResp.Encoder.getInstance());
        decoderMap.put(S_C_ScenePlayerListReq.CODE, S_C_ScenePlayerListReq.Decoder.getInstance());
        encoderMap.put(S_C_ScenePlayerListReq.CODE, S_C_ScenePlayerListReq.Encoder.getInstance());
        decoderMap.put(S_C_ScenePlayerEnterReq.CODE, S_C_ScenePlayerEnterReq.Decoder.getInstance());
        encoderMap.put(S_C_ScenePlayerEnterReq.CODE, S_C_ScenePlayerEnterReq.Encoder.getInstance());
        decoderMap.put(C_S_SceneSwitchReq.CODE, C_S_SceneSwitchReq.Decoder.getInstance());
        encoderMap.put(C_S_SceneSwitchReq.CODE, C_S_SceneSwitchReq.Encoder.getInstance());
        decoderMap.put(S_C_ScenePlayerLeaveReq.CODE, S_C_ScenePlayerLeaveReq.Decoder.getInstance());
        encoderMap.put(S_C_ScenePlayerLeaveReq.CODE, S_C_ScenePlayerLeaveReq.Encoder.getInstance());
        decoderMap.put(C_S_SceneMoveReq.CODE, C_S_SceneMoveReq.Decoder.getInstance());
        encoderMap.put(C_S_SceneMoveReq.CODE, C_S_SceneMoveReq.Encoder.getInstance());
        decoderMap.put(S_C_ScenePlayerMoveReq.CODE, S_C_ScenePlayerMoveReq.Decoder.getInstance());
        encoderMap.put(S_C_ScenePlayerMoveReq.CODE, S_C_ScenePlayerMoveReq.Encoder.getInstance());
        decoderMap.put(S_C_ScenePlayerChangePosResp.CODE, S_C_ScenePlayerChangePosResp.Decoder.getInstance());
        encoderMap.put(S_C_ScenePlayerChangePosResp.CODE, S_C_ScenePlayerChangePosResp.Encoder.getInstance());
        decoderMap.put(S_C_SceneSelfChangePosResp.CODE, S_C_SceneSelfChangePosResp.Decoder.getInstance());
        encoderMap.put(S_C_SceneSelfChangePosResp.CODE, S_C_SceneSelfChangePosResp.Encoder.getInstance());
        decoderMap.put(S_C_BagUpdateReq.CODE, S_C_BagUpdateReq.Decoder.getInstance());
        encoderMap.put(S_C_BagUpdateReq.CODE, S_C_BagUpdateReq.Encoder.getInstance());
        decoderMap.put(S_C_BagItemUpdateReq.CODE, S_C_BagItemUpdateReq.Decoder.getInstance());
        encoderMap.put(S_C_BagItemUpdateReq.CODE, S_C_BagItemUpdateReq.Encoder.getInstance());
        decoderMap.put(C_S_AddItemToPrimBagReq.CODE, C_S_AddItemToPrimBagReq.Decoder.getInstance());
        encoderMap.put(C_S_AddItemToPrimBagReq.CODE, C_S_AddItemToPrimBagReq.Encoder.getInstance());
        decoderMap.put(C_S_DropItemReq.CODE, C_S_DropItemReq.Decoder.getInstance());
        encoderMap.put(C_S_DropItemReq.CODE, C_S_DropItemReq.Encoder.getInstance());
        decoderMap.put(C_S_PackUpBagReq.CODE, C_S_PackUpBagReq.Decoder.getInstance());
        encoderMap.put(C_S_PackUpBagReq.CODE, C_S_PackUpBagReq.Encoder.getInstance());
        decoderMap.put(C_S_MoveItemReq.CODE, C_S_MoveItemReq.Decoder.getInstance());
        encoderMap.put(C_S_MoveItemReq.CODE, C_S_MoveItemReq.Encoder.getInstance());
        decoderMap.put(S_C_ItemGetResp.CODE, S_C_ItemGetResp.Decoder.getInstance());
        encoderMap.put(S_C_ItemGetResp.CODE, S_C_ItemGetResp.Encoder.getInstance());
        decoderMap.put(C_S_ItemUseReq.CODE, C_S_ItemUseReq.Decoder.getInstance());
        encoderMap.put(C_S_ItemUseReq.CODE, C_S_ItemUseReq.Encoder.getInstance());
        decoderMap.put(S_C_ItemUseReelResp.CODE, S_C_ItemUseReelResp.Decoder.getInstance());
        encoderMap.put(S_C_ItemUseReelResp.CODE, S_C_ItemUseReelResp.Encoder.getInstance());
        decoderMap.put(S_C_BagItemDelResp.CODE, S_C_BagItemDelResp.Decoder.getInstance());
        encoderMap.put(S_C_BagItemDelResp.CODE, S_C_BagItemDelResp.Encoder.getInstance());
        decoderMap.put(C_S_AcceptQuestReq.CODE, C_S_AcceptQuestReq.Decoder.getInstance());
        encoderMap.put(C_S_AcceptQuestReq.CODE, C_S_AcceptQuestReq.Encoder.getInstance());
        decoderMap.put(S_C_QuestListResp.CODE, S_C_QuestListResp.Decoder.getInstance());
        encoderMap.put(S_C_QuestListResp.CODE, S_C_QuestListResp.Encoder.getInstance());
        decoderMap.put(S_C_QuestUpdateResp.CODE, S_C_QuestUpdateResp.Decoder.getInstance());
        encoderMap.put(S_C_QuestUpdateResp.CODE, S_C_QuestUpdateResp.Encoder.getInstance());
        decoderMap.put(C_S_CancelQuestReq.CODE, C_S_CancelQuestReq.Decoder.getInstance());
        encoderMap.put(C_S_CancelQuestReq.CODE, C_S_CancelQuestReq.Encoder.getInstance());
        decoderMap.put(C_S_FinishQuestReq.CODE, C_S_FinishQuestReq.Decoder.getInstance());
        encoderMap.put(C_S_FinishQuestReq.CODE, C_S_FinishQuestReq.Encoder.getInstance());
        decoderMap.put(S_C_FinishQuestResp.CODE, S_C_FinishQuestResp.Decoder.getInstance());
        encoderMap.put(S_C_FinishQuestResp.CODE, S_C_FinishQuestResp.Encoder.getInstance());
        decoderMap.put(C_S_PlayerCreateReq.CODE, C_S_PlayerCreateReq.Decoder.getInstance());
        encoderMap.put(C_S_PlayerCreateReq.CODE, C_S_PlayerCreateReq.Encoder.getInstance());
        decoderMap.put(S_C_PlayerCreateResp.CODE, S_C_PlayerCreateResp.Decoder.getInstance());
        encoderMap.put(S_C_PlayerCreateResp.CODE, S_C_PlayerCreateResp.Encoder.getInstance());
        decoderMap.put(S_C_PlayerNoneResp.CODE, S_C_PlayerNoneResp.Decoder.getInstance());
        encoderMap.put(S_C_PlayerNoneResp.CODE, S_C_PlayerNoneResp.Encoder.getInstance());
        decoderMap.put(S_C_PlayerInfoResp.CODE, S_C_PlayerInfoResp.Decoder.getInstance());
        encoderMap.put(S_C_PlayerInfoResp.CODE, S_C_PlayerInfoResp.Encoder.getInstance());
        decoderMap.put(S_C_PlayerLevelUpResp.CODE, S_C_PlayerLevelUpResp.Decoder.getInstance());
        encoderMap.put(S_C_PlayerLevelUpResp.CODE, S_C_PlayerLevelUpResp.Encoder.getInstance());
        decoderMap.put(S_C_PlayerAddExpResp.CODE, S_C_PlayerAddExpResp.Decoder.getInstance());
        encoderMap.put(S_C_PlayerAddExpResp.CODE, S_C_PlayerAddExpResp.Encoder.getInstance());
        decoderMap.put(S_C_PlayerAddCurrencyResp.CODE, S_C_PlayerAddCurrencyResp.Decoder.getInstance());
        encoderMap.put(S_C_PlayerAddCurrencyResp.CODE, S_C_PlayerAddCurrencyResp.Encoder.getInstance());
        decoderMap.put(C_S_LoginReq.CODE, C_S_LoginReq.Decoder.getInstance());
        encoderMap.put(C_S_LoginReq.CODE, C_S_LoginReq.Encoder.getInstance());
        decoderMap.put(S_C_LoginResultResp.CODE, S_C_LoginResultResp.Decoder.getInstance());
        encoderMap.put(S_C_LoginResultResp.CODE, S_C_LoginResultResp.Encoder.getInstance());
        decoderMap.put(S_C_OnlineDiggedReq.CODE, S_C_OnlineDiggedReq.Decoder.getInstance());
        encoderMap.put(S_C_OnlineDiggedReq.CODE, S_C_OnlineDiggedReq.Encoder.getInstance());
        decoderMap.put(C_S_LogoutReq.CODE, C_S_LogoutReq.Decoder.getInstance());
        encoderMap.put(C_S_LogoutReq.CODE, C_S_LogoutReq.Encoder.getInstance());
        decoderMap.put(S_C_LogoutResp.CODE, S_C_LogoutResp.Decoder.getInstance());
        encoderMap.put(S_C_LogoutResp.CODE, S_C_LogoutResp.Encoder.getInstance());
        decoderMap.put(C_S_RegisterReq.CODE, C_S_RegisterReq.Decoder.getInstance());
        encoderMap.put(C_S_RegisterReq.CODE, C_S_RegisterReq.Encoder.getInstance());
        decoderMap.put(S_C_RegisterResp.CODE, S_C_RegisterResp.Decoder.getInstance());
        encoderMap.put(S_C_RegisterResp.CODE, S_C_RegisterResp.Encoder.getInstance());
        decoderMap.put(C_S_ActivityListReq.CODE, C_S_ActivityListReq.Decoder.getInstance());
        encoderMap.put(C_S_ActivityListReq.CODE, C_S_ActivityListReq.Encoder.getInstance());
        decoderMap.put(S_C_ActivityListResp.CODE, S_C_ActivityListResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityListResp.CODE, S_C_ActivityListResp.Encoder.getInstance());
        decoderMap.put(S_C_ActivityStartResp.CODE, S_C_ActivityStartResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityStartResp.CODE, S_C_ActivityStartResp.Encoder.getInstance());
        decoderMap.put(S_C_ActivityEndResp.CODE, S_C_ActivityEndResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityEndResp.CODE, S_C_ActivityEndResp.Encoder.getInstance());
        decoderMap.put(C_S_ActivityJoinReq.CODE, C_S_ActivityJoinReq.Decoder.getInstance());
        encoderMap.put(C_S_ActivityJoinReq.CODE, C_S_ActivityJoinReq.Encoder.getInstance());
        decoderMap.put(C_S_ActivityLeaveReq.CODE, C_S_ActivityLeaveReq.Decoder.getInstance());
        encoderMap.put(C_S_ActivityLeaveReq.CODE, C_S_ActivityLeaveReq.Encoder.getInstance());
        decoderMap.put(S_C_ActivityBossInfoResp.CODE, S_C_ActivityBossInfoResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityBossInfoResp.CODE, S_C_ActivityBossInfoResp.Encoder.getInstance());
        decoderMap.put(S_C_ActivityBossRankResp.CODE, S_C_ActivityBossRankResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityBossRankResp.CODE, S_C_ActivityBossRankResp.Encoder.getInstance());
        decoderMap.put(C_S_ActivityBossAttackReq.CODE, C_S_ActivityBossAttackReq.Decoder.getInstance());
        encoderMap.put(C_S_ActivityBossAttackReq.CODE, C_S_ActivityBossAttackReq.Encoder.getInstance());
        decoderMap.put(S_C_ActivityBossCanMoveResp.CODE, S_C_ActivityBossCanMoveResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityBossCanMoveResp.CODE, S_C_ActivityBossCanMoveResp.Encoder.getInstance());
        decoderMap.put(S_C_ActivityBossPlayerInfoResp.CODE, S_C_ActivityBossPlayerInfoResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityBossPlayerInfoResp.CODE, S_C_ActivityBossPlayerInfoResp.Encoder.getInstance());
        decoderMap.put(C_S_ActivityBossEncourageSoulReq.CODE, C_S_ActivityBossEncourageSoulReq.Decoder.getInstance());
        encoderMap.put(C_S_ActivityBossEncourageSoulReq.CODE, C_S_ActivityBossEncourageSoulReq.Encoder.getInstance());
        decoderMap.put(S_C_ActivityBossEncourageSoulResp.CODE, S_C_ActivityBossEncourageSoulResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityBossEncourageSoulResp.CODE, S_C_ActivityBossEncourageSoulResp.Encoder.getInstance());
        decoderMap.put(C_S_ActivityBossEncourageWoodReq.CODE, C_S_ActivityBossEncourageWoodReq.Decoder.getInstance());
        encoderMap.put(C_S_ActivityBossEncourageWoodReq.CODE, C_S_ActivityBossEncourageWoodReq.Encoder.getInstance());
        decoderMap.put(S_C_ActivityBossEncourageWoodResp.CODE, S_C_ActivityBossEncourageWoodResp.Decoder.getInstance());
        encoderMap.put(S_C_ActivityBossEncourageWoodResp.CODE, S_C_ActivityBossEncourageWoodResp.Encoder.getInstance());
        decoderMap.put(C_S_ArenaDetailReq.CODE, C_S_ArenaDetailReq.Decoder.getInstance());
        encoderMap.put(C_S_ArenaDetailReq.CODE, C_S_ArenaDetailReq.Encoder.getInstance());
        decoderMap.put(C_S_ArenaDetailResp.CODE, C_S_ArenaDetailResp.Decoder.getInstance());
        encoderMap.put(C_S_ArenaDetailResp.CODE, C_S_ArenaDetailResp.Encoder.getInstance());
        decoderMap.put(C_S_ArenaAddChallengeReq.CODE, C_S_ArenaAddChallengeReq.Decoder.getInstance());
        encoderMap.put(C_S_ArenaAddChallengeReq.CODE, C_S_ArenaAddChallengeReq.Encoder.getInstance());
        decoderMap.put(S_C_ArenaAddChallengeResp.CODE, S_C_ArenaAddChallengeResp.Decoder.getInstance());
        encoderMap.put(S_C_ArenaAddChallengeResp.CODE, S_C_ArenaAddChallengeResp.Encoder.getInstance());
        decoderMap.put(C_S_ArenaClearChallengeCDReq.CODE, C_S_ArenaClearChallengeCDReq.Decoder.getInstance());
        encoderMap.put(C_S_ArenaClearChallengeCDReq.CODE, C_S_ArenaClearChallengeCDReq.Encoder.getInstance());
        decoderMap.put(S_C_ArenaClearChallengeCDResp.CODE, S_C_ArenaClearChallengeCDResp.Decoder.getInstance());
        encoderMap.put(S_C_ArenaClearChallengeCDResp.CODE, S_C_ArenaClearChallengeCDResp.Encoder.getInstance());
        decoderMap.put(C_S_ArenaBattleReq.CODE, C_S_ArenaBattleReq.Decoder.getInstance());
        encoderMap.put(C_S_ArenaBattleReq.CODE, C_S_ArenaBattleReq.Encoder.getInstance());
        decoderMap.put(S_C_PartnerListResp.CODE, S_C_PartnerListResp.Decoder.getInstance());
        encoderMap.put(S_C_PartnerListResp.CODE, S_C_PartnerListResp.Encoder.getInstance());
        decoderMap.put(C_S_PartnerInnListReq.CODE, C_S_PartnerInnListReq.Decoder.getInstance());
        encoderMap.put(C_S_PartnerInnListReq.CODE, C_S_PartnerInnListReq.Encoder.getInstance());
        decoderMap.put(S_C_PartnerInnListResp.CODE, S_C_PartnerInnListResp.Decoder.getInstance());
        encoderMap.put(S_C_PartnerInnListResp.CODE, S_C_PartnerInnListResp.Encoder.getInstance());
        decoderMap.put(C_S_PartnerRecruitReq.CODE, C_S_PartnerRecruitReq.Decoder.getInstance());
        encoderMap.put(C_S_PartnerRecruitReq.CODE, C_S_PartnerRecruitReq.Encoder.getInstance());
        decoderMap.put(S_C_PartnerAddResp.CODE, S_C_PartnerAddResp.Decoder.getInstance());
        encoderMap.put(S_C_PartnerAddResp.CODE, S_C_PartnerAddResp.Encoder.getInstance());
        decoderMap.put(C_S_PartnerFireReq.CODE, C_S_PartnerFireReq.Decoder.getInstance());
        encoderMap.put(C_S_PartnerFireReq.CODE, C_S_PartnerFireReq.Encoder.getInstance());
        decoderMap.put(S_C_PartnerDelResp.CODE, S_C_PartnerDelResp.Decoder.getInstance());
        encoderMap.put(S_C_PartnerDelResp.CODE, S_C_PartnerDelResp.Encoder.getInstance());

    }

    public static AbstractDecoder getDecoder(Short msgId) {
        return decoderMap.get(msgId);
    }

    public static AbstractEncoder getEncoder(Short msgId) {
        return encoderMap.get(msgId);
    }
}
