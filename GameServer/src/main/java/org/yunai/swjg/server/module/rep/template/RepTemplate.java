package org.yunai.swjg.server.module.rep.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.rep.RepDef;
import org.yunai.swjg.server.module.scene.core.template.SceneTemplate;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 副本信息模版
 * User: yunai
 * Date: 13-5-16
 * Time: 上午12:20
 */
public class RepTemplate {

    /**
     * 副本编号
     */
    private Integer id;
    /**
     * 副本标题
     */
    private String title;
    /**
     * 副本对应场景编号
     */
    private SceneTemplate sceneTemplate;
    /**
     * 副本类型<br />
     * {@link RepDef.Type}
     */
    private RepDef.Type type;
    /**
     * 怪物经验奖励
     */
    private Integer monsterBonusExp;
    /**
     * 通关奖励经验
     */
    private Integer bonusExp;
    /**
     * 通关奖励历练
     */
    private Integer bonusSoul;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public RepDef.Type getType() {
        return type;
    }

    public Integer getMonsterBonusExp() {
        return monsterBonusExp;
    }

    public Integer getBonusExp() {
        return bonusExp;
    }

    public Integer getBonusSoul() {
        return bonusSoul;
    }

    public SceneTemplate getSceneTemplate() {
        return sceneTemplate;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, RepTemplate> templates;
    private static Map<Integer, RepTemplate> templates2;

    public static RepTemplate get(Integer id) {
        return templates.get(id);
    }

    public static RepTemplate getBySceneId(Integer sceneId) {
        return templates2.get(sceneId);
    }

    public static void load() {
        CsvReader reader = null;
        // 副本模版
        templates = new HashMap<>();
        templates2 = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/rep/rep_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                RepTemplate template = read(reader);
                templates.put(template.getId(), template);
                templates2.put(template.getSceneTemplate().getId(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // TODO check
    }

    private static RepTemplate read(CsvReader reader) throws IOException {
        RepTemplate template = new RepTemplate();
        template.id = CsvUtil.getInt(reader, "id", 0);
        template.title = CsvUtil.getString(reader, "title", "");
        template.sceneTemplate = SceneTemplate.get(CsvUtil.getInt(reader, "sceneId", 0));
        template.type = RepDef.Type.valueOf(CsvUtil.getShort(reader, "type", (short) 0));
        template.monsterBonusExp = CsvUtil.getInt(reader, "monsterBonusExp", 0);
        template.bonusExp = CsvUtil.getInt(reader, "bonusExp", 0);
        template.bonusSoul = CsvUtil.getInt(reader, "bonusSoul", 0);
        return template;
    }
}