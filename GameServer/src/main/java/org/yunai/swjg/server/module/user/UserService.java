package org.yunai.swjg.server.module.user;

import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.user.operation.UserLoginOperation;
import org.yunai.yfserver.async.IIoOperationService;

import javax.annotation.Resource;

/**
 * 用户逻辑
 * User: yunai
 * Date: 13-3-28
 * Time: 下午8:20
 */
@Service
public class UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private IIoOperationService ioOperationService;

    public void auth(Online online, Short serverId, String userName, String password) {
        ioOperationService.asyncExecute(new UserLoginOperation(online.getSession(), serverId, userName, password));
    }
}
