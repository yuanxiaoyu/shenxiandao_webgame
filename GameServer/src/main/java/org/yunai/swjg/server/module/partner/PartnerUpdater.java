package org.yunai.swjg.server.module.partner;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.partner.vo.Partner;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import javax.annotation.Resource;

/**
 * 伙伴持久化更新器
 * User: yunai
 * Date: 13-5-31
 * Time: 上午1:27
 */
@Component
public class PartnerUpdater implements PersistenceObjectUpdater {

    @Resource
    private PartnerMapper partnerMapper;
    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void save(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<>((Partner) obj, partnerMapper));
    }

    @Override
    public void delete(PersistenceObject<?, ?> obj) {
        throw new UnsupportedOperationException("伙伴信息不支持delete.");
    }
}