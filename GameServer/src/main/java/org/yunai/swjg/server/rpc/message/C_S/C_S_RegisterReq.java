package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21406】: 注册请求
 */
public class C_S_RegisterReq extends GameMessage {
    public static final short CODE = 21406;

    /**
     * 密码
     */
    private String password;
    /**
     * 用户名
     */
    private String userName;

    public C_S_RegisterReq() {
    }

    public C_S_RegisterReq(String password, String userName) {
        this.password = password;
        this.userName = userName;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_RegisterReq struct = new C_S_RegisterReq();
            struct.setPassword(getString(byteArray));
            struct.setUserName(getString(byteArray));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_RegisterReq struct = (C_S_RegisterReq) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[] passwordBytes = convertString(byteArray, struct.getPassword());
            byte[] userNameBytes = convertString(byteArray, struct.getUserName());
            byteArray.create();
            putString(byteArray, passwordBytes);
            putString(byteArray, userNameBytes);
            return byteArray;
        }
    }
}