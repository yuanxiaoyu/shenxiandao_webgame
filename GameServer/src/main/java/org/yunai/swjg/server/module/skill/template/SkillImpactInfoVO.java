package org.yunai.swjg.server.module.skill.template;

import org.yunai.swjg.server.module.skill.SkillDef;
import org.yunai.yfserver.util.MathUtils;
import org.yunai.yfserver.util.StringUtils;

import java.util.List;

/**
 * 技能影响信息模版<br />
 * 该模版不同于其他模版，带了业务方法
 * User: yunai
 * Date: 13-6-16
 * Time: 上午10:55
 */
public class SkillImpactInfoVO {

    /**
     * 临时buff的回合数标记
     */
    public static final int ROUND_TEMP = 0;

    /**
     * 生效概率基数
     */
    public static final int RATE_MAX = 10000;

    private SkillImpactTemplate template;
    private SkillDef.ImpactTime time;
    private int rate;
    private int round;
    private SkillDef.ImpactTarget target;
    /**
     * 拓展参数数组。
     * <pre>
     *     当不需要拓展参数时，需要将值填为-1
     *     像增加属性百分比之类的，是需要该参数的。
     *     但是，比如说晕眩、睡眠等，就不需要该参数。
     * </pre>
     */
    private int[] params;

    public SkillDef.ImpactTime getTime() {
        return time;
    }

    public int getRound() {
        return round;
    }

    public SkillDef.ImpactTarget getTarget() {
        return target;
    }

    public int[] getParams() {
        return params;
    }

    public List<SkillDef.Effect> getEffects() {
        return template.getEffects();
    }

    public int getParam(int i) {
        return params[i];
    }

    public SkillDef.Effect getEffect(int i) {
        return template.getEffects().get(i);
    }

//    public List<SkillDef.Effect> getBuffDebuffEffects() {
//        return buffDebuffEffects;
//    }
//
//    public List<SkillDef.Effect> getHotDotEffects() {
//        return hotDotEffects;
//    }
//
//    public List<SkillDef.Effect> getFunctionEffects() {
//        return functionEffects;
//    }

    private SkillImpactInfoVO() {
    }

    // ==================== 非set/get方法 ====================

    public static SkillImpactInfoVO parse(String str) {
        String[] strs = StringUtils.split(str, "\\[");
        if (strs.length <= 1) {
            throw new IllegalArgumentException("参数错误：" + str);
        }
        // 前面部分
        SkillImpactInfoVO infoTemplate = new SkillImpactInfoVO();
        int[] befores = StringUtils.splitInt(strs[0], "\\|");
        if (befores.length != 5) {
            throw new IllegalArgumentException("参数错误：" + str);
        }
        infoTemplate.template = SkillImpactTemplate.getItemTemplate(befores[0]);
        if (infoTemplate.template == null) {
            throw new IllegalArgumentException("参数错误：" + str);
        }
        infoTemplate.time = SkillDef.ImpactTime.valueOf(befores[1]);
        infoTemplate.rate = befores[2];
        infoTemplate.round = befores[3];
        infoTemplate.target = SkillDef.ImpactTarget.valueOf(befores[4]);
        // 后面部分
        strs[1] = strs[1].replace("]", "");
        int[] afters = StringUtils.splitInt(strs[1], "\\,");
        if (afters.length != infoTemplate.template.getEffects().size()) {
            throw new IllegalArgumentException("参数错误：" + str);
        }
        infoTemplate.params = afters;
        return infoTemplate;
    }

    /**
     * @return 是否触发
     */
    public boolean effect() {
        return MathUtils.nextInt(RATE_MAX) < rate;
    }
}