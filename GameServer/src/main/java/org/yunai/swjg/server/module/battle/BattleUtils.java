package org.yunai.swjg.server.module.battle;

/**
 * 战斗工具类
 * User: yunai
 * Date: 13-6-17
 * Time: 上午9:35
 */
public class BattleUtils {

    /**
     * buff增加百分比效果的基础值
     */
    public static final double BUFF_RATE_BASE = 10000D;
    /**
     * 战斗最大回合
     */
    public static final int ROUND_MAX = 200;

    /**
     * 攻击队伍起始战斗单元编号
     */
    public static final int INDEX_BEGIN_ATTACK_TEAM = 1;
    /**
     * 防御队伍起始战斗单元编号
     */
    public static final int INDEX_BEGIN_DEFENSE_TEAM = 10;

    /**
     * 战斗单元在{@link org.yunai.swjg.server.module.skill.SkillDef.Scope#HANG}遍历的顺序<br />
     * 第一个[]：战斗单元所处行<br />
     * 第二个[]：战斗单元遍历的顺序
     */
    public static final int SCOPE_HANG[][] = {{3, 2, 1}, {1, 2, 3}, {2, 1, 3}};
    /**
     * 每行战斗单元个数
     */
    public static final int HANG_COUNT = 3;
    /**
     * 每列战斗单元个数
     */
    public static final int LIE_COUNT = 3;
}