package org.yunai.swjg.server.module.item;

import org.yunai.swjg.server.entity.ItemEntity;
import org.yunai.yfserver.persistence.orm.mybatis.DeleteMapper;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

import java.util.List;

/**
 * Item
 * User: yunai
 * Date: 13-4-9
 * Time: 下午4:52
 */
public interface ItemMapper extends Mapper, SaveMapper<ItemEntity>, DeleteMapper<ItemEntity> {

    /**
     * 获得玩家所有道具
     *
     * @param playerId 玩家编号
     * @return 道具数组
     */
    List<ItemEntity> selectList(int playerId);

    /**
     * 插入数据
     * @param entity 数据
     */
    @Override
    void insert(ItemEntity entity);

    /**
     * 更新数据
     * @param entity 数据
     */
    @Override
    void update(ItemEntity entity);

    /**
     * 删除数据
     * @param entity 数据
     */
    @Override
    void delete(ItemEntity entity);
}
