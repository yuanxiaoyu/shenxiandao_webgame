package org.yunai.swjg.server.module.battle;

/**
 * 战斗定义
 * User: yunai
 * Date: 13-6-15
 * Time: 下午10:04
 */
public interface BattleDef {

    public static enum Team {
        ATTACK,
        DEFENSE
    }

    /**
     * 行为类型枚举
     */
    public static enum ActionType {
        /**
         * 技能
         */
        SKILL((byte) 1),
        /**
         * 死亡
         */
        DEATH((byte) 2);

        private final byte type;

        private ActionType(byte type) {
            this.type = type;
        }

        public byte get() {
            return type;
        }
    }

    /**
     * 对行为目标操作类型枚举
     */
    public static enum ActionTargetOpType {

        /**
         * 丢失
         */
        MISS((byte) 1),
        /**
         * 命中
         */
        HIT((byte) 2),
        /**
         * 暴击
         */
        CRIT((byte) 3),
        /**
         * 秒杀
         */
        SEC_KILL((byte) 4),
        ;

        private final byte opType;

        private ActionTargetOpType(byte opByte) {
            this.opType = opByte;
        }

        public byte get() {
            return opType;
        }
    }
}
