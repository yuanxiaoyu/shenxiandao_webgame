package org.yunai.swjg.server.module.monster.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 怪物模版
 * User: yunai
 * Date: 13-5-16
 * Time: 上午12:24
 */
public class MonsterTemplate {

    /**
     * 怪物编号
     */
    private Integer id;
    /**
     * 怪物名
     */
    private String name;
    /**
     * 技能
     */
    private Short skill;
    /**
     * 普通攻击
     */
    private int strengthAttack;
    /**
     * 绝技攻击
     */
    private int stuntAttack;
    /**
     * 法术攻击
     */
    private int magicAttack;
    /**
     * 普通防御
     */
    private int strengthDefense;
    /**
     * 绝技防御
     */
    private int stuntDefense;
    /**
     * 法术防御
     */
    private int magicDefense;
    /**
     * 血
     */
    private int hp;
    /**
     * 暴击，万分比
     */
    private int crit;
    /**
     * 命中，万分比
     */
    private int hit;
    /**
     * 破击，万分比
     */
    private int poJi;
    /**
     * 必杀，万分比
     */
    private int biSha;
    /**
     * 韧性，万分比
     */
    private int toughness;
    /**
     * 闪避，万分比
     */
    private int dodge;
    /**
     * 格挡，万分比
     */
    private int block;
    /**
     * 先攻
     */
    private int xianGong;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Short getSkill() {
        return skill;
    }

    public int getStrengthAttack() {
        return strengthAttack;
    }

    public int getStuntAttack() {
        return stuntAttack;
    }

    public int getMagicAttack() {
        return magicAttack;
    }

    public int getStrengthDefense() {
        return strengthDefense;
    }

    public int getStuntDefense() {
        return stuntDefense;
    }

    public int getMagicDefense() {
        return magicDefense;
    }

    public int getHp() {
        return hp;
    }

    public int getXianGong() {
        return xianGong;
    }

    public int getCrit() {
        return crit;
    }

    public int getHit() {
        return hit;
    }

    public int getPoJi() {
        return poJi;
    }

    public int getBiSha() {
        return biSha;
    }

    public int getToughness() {
        return toughness;
    }

    public int getDodge() {
        return dodge;
    }

    public int getBlock() {
        return block;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, MonsterTemplate> templates;

    public static MonsterTemplate get(Integer id) {
        return templates.get(id);
    }

    public static void load() {
        CsvReader reader = null;
        // 怪物模版
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/monster/monster_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                MonsterTemplate template = read(reader);
                templates.put(template.getId(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // TODO check
    }

    private static MonsterTemplate read(CsvReader reader) throws IOException {
        MonsterTemplate template = new MonsterTemplate();
        template.id = CsvUtil.getInt(reader, "id", 0);
        template.name = CsvUtil.getString(reader, "name", "");
        template.skill = CsvUtil.getShort(reader, "skill", (short) 0);
        template.strengthAttack = CsvUtil.getInt(reader, "strengthAttack", 0);
        template.stuntAttack = CsvUtil.getInt(reader, "stuntAttack", 0);
        template.magicAttack = CsvUtil.getInt(reader, "magicAttack", 0);
        template.strengthDefense = CsvUtil.getInt(reader, "strengthDefense", 0);
        template.stuntDefense = CsvUtil.getInt(reader, "stuntDefense", 0);
        template.magicDefense = CsvUtil.getInt(reader, "magicDefense", 0);
        template.hp = CsvUtil.getInt(reader, "hp", 0);
        template.xianGong = CsvUtil.getInt(reader, "xianGong", 0);
        template.crit = CsvUtil.getInt(reader, "crit", 0);
        template.hit = CsvUtil.getInt(reader, "hit", 0);
        template.poJi = CsvUtil.getInt(reader, "poJi", 0);
        template.biSha = CsvUtil.getInt(reader, "biSha", 0);
        template.toughness = CsvUtil.getInt(reader, "toughness", 0);
        template.dodge = CsvUtil.getInt(reader, "dodge", 0);
        template.block =CsvUtil.getInt(reader, "block", 0);
        return template;
    }
}
