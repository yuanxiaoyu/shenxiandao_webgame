package org.yunai.swjg.server.module.currency;

import org.yunai.swjg.server.core.constants.PlayerConstants;
import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 货币枚举
 * User: yunai
 * Date: 13-4-7
 * Time: 下午9:49
 */
public enum Currency implements IndexedEnum {
    /**
     * 游戏币
     */
    COIN(1, PlayerConstants.CURRENCY_COIN_MAX),
    /**
     * 元宝
     */
    GOLD(2, PlayerConstants.CURRENCY_GOLD_MAX);

    /**
     * 货币编号
     */
    private final int index;
    /**
     * 货币上限
     */
    private final int max;

    private Currency(int index, int max) {
        this.index = index;
        this.max = max;
    }

    public static final List<Currency> VALUES = IndexedEnum.Util.toIndexes(Currency.values());

    public static Currency valueOf(Integer index) {
        return IndexedEnum.Util.valueOf(VALUES, index);
    }

    @Override
    public int getIndex() {
        return index;
    }

    public int getMax() {
        return max;
    }

    /**
     * 将index转为byte返回
     *
     * @return index的byte范围的值
     */
    public byte getValue() {
        return (byte) index;
    }
}
