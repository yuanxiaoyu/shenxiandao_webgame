package quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-5-25
 * Time: 上午3:52
 * To change this template use File | Settings | File Templates.
 */
public class HelloJob implements Job {

    public HelloJob() {

    }

    public void execute(JobExecutionContext context)
            throws JobExecutionException {

        System.out.println(" 咫尺天涯: " + new Date());

    }
}
