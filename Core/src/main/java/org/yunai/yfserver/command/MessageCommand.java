package org.yunai.yfserver.command;

import org.apache.mina.core.session.IoSession;

/**
 * 消息命令
 * User: yunai
 * Date: 13-3-15
 * Time: 上午9:43
 */
public interface MessageCommand<E> extends Command {

    void execute(IoSession session, E message);
}
