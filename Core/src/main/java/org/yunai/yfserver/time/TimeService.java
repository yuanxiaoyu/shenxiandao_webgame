package org.yunai.yfserver.time;

/**
 * 提交时间相关服务接口
 * User: yunai
 * Date: 13-4-27
 * Time: 下午5:05
 */
public interface TimeService {

    /**
     * @return 当前时间
     */
    long now();

    /**
     * 更新当前时间<br />
     * 只有在当前时间服务使用了缓存策略的情况下菜需要调用此方法
     */
    void update();

    /**
     * @param time 指定时间
     * @return 是否到达某个时间
     */
    boolean timeUp(long time);

    /**
     * @param time 指定时间
     * @return 获得指定时间与当前时间的时间差
     */
    long getInterval(long time);
}
