package org.yunai.yfserver.server;

import org.yunai.yfserver.message.IMessage;

/**
 * 消息处理器接口
 * User: yunai
 * Date: 13-3-25
 * Time: 上午10:39
 */
public interface IMessageProcessor {

    /**
     * 开始消息处理器
     */
    void start();

    /**
     * 停止消息处理器
     */
    void stop();

    /**
     * 加入消息到消息队列
     * @param message 消息
     */
    void put(IMessage message);

    /**
     * 消息队列是否满了
     * @return 是否满
     */
    boolean isFull();
}
