package org.yunai.yfserver.async;

/**
 * 同步执行Io操作策略
 * User: yunai
 * Date: 13-3-28
 * Time: 下午3:20
 */
public class SyncIoOperation {

    /** IO具体操作 */
    private final IIoOperation operation;
    /** 操作当前状态 */
    private IIoOperation.State state;

    public SyncIoOperation(IIoOperation operation) {
        this.operation = operation;
        this.state = IIoOperation.State.INITIALIZED;
    }

    /**
     * 同步执行Io操作<br />
     * 按照state顺序执行下去
     */
    public void execute() {
        state = operation.doStart();
        if (state == IIoOperation.State.STARTED) {
            state = operation.doIo();
        }
        if (state == IIoOperation.State.IO_DONE) {
            state = operation.doFinish();
        }
    }
}
