package org.yunai.yfserver.util;

/**
 * 时间工具类
 * User: yunai
 * Date: 13-5-22
 * Time: 下午4:28
 */
public class TimeUtils {

    /**
     * 1秒 转化为 毫秒
     */
    public static final long SECOND = 1 * 1000;
    /**
     * 1分 转化为 毫秒
     */
    public static final long MINUTE = 1 * 60 * SECOND;

}
